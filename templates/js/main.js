var itemList = document.getElementById("myUL")
itemList.addEventListener("click", removeItem)

fetch("/todo")
    .then((res) => res.json())
    .then(res => display(res))

function display(data) {
    for (var i = 0; i < data.length; i++) {
        var li = document.createElement('li');
        li.id = data[i].ID

        li.appendChild(document.createTextNode(data[i].title))

        var deleteBtn = document.createElement('button')

        deleteBtn.className = 'btn btn-danger btn-sm float-right delete';
        deleteBtn.appendChild(document.createTextNode('X'));
        li.appendChild(deleteBtn)
        itemList.appendChild(li);
    }
}

function removeItem(e) {
    var id = e.target.parentNode.id
    var li = e.target.parentElement
    console.log(li)
    itemList.removeChild(li)
    fetch('/todo/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
}

function newElement(e) {
    var inputValue = document.getElementById("myInput").value
    var li = document.createElement('li');
    if (inputValue === '') {
        alert("You must write something!");
    } else {
        fetch('/todo/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title: inputValue
                })
            })
            .then((res) => res.json())
            .then(obj => {
                li.id = obj.ID
                console.log(obj)
            })

        li.appendChild(document.createTextNode(inputValue))

        var deleteBtn = document.createElement('button')

        deleteBtn.className = 'btn btn-danger btn-sm float-right delete';
        deleteBtn.appendChild(document.createTextNode('X'));
        li.appendChild(deleteBtn)
        itemList.appendChild(li);
        document.getElementById("myInput").value = ""
    }
}
