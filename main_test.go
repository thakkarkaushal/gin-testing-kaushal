package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string, body io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, body)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestGetAllTodo(t *testing.T) {
	router := SetupRouter()
	w := performRequest(router, "GET", "/todo", nil)
	assert.Equal(t, http.StatusOK, w.Code)
	fmt.Println(string([]byte(w.Body.String())))
}

func TestAddTodo(t *testing.T) {
	router := SetupRouter()
	requestBody, err := json.Marshal(map[string]string{
		"title": "Testing the post request",
	})
	if err != nil {
		panic(err)
	}
	w := performRequest(router, "POST", "/todo", bytes.NewBuffer(requestBody))
	assert.Equal(t, http.StatusOK, w.Code)
	fmt.Println(string([]byte(w.Body.String())))
}

func TestDeleteTodo(t *testing.T) {
	router := SetupRouter()
	id := "62"
	w := performRequest(router, "DELETE", "/todo/"+id, nil)
	assert.Equal(t, http.StatusOK, w.Code)
	fmt.Println(string([]byte(w.Body.String())))
}

func TestPutTodo(t *testing.T) {
	router := SetupRouter()
	requestBody, err := json.Marshal(map[string]string{
		"title": "Testing the put request",
	})
	if err != nil {
		panic(err)
	}
	id := "64"
	w := performRequest(router, "PUT", "/todo/"+id, bytes.NewBuffer(requestBody))
	assert.Equal(t, http.StatusOK, w.Code)
	fmt.Println(string([]byte(w.Body.String())))
}
