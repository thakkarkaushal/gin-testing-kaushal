package models

import (
	"github.com/jinzhu/gorm"
)

// TodoStruct to store the title in database
type TodoStruct struct {
	gorm.Model
	Title string `json:"title" gorm:"type:varchar(200)"`
}

// User to store email & passowrd in database
type User struct {
	gorm.Model
	Name     string `json:"name" gorm:"type:varchar(100)"`
	Email    string `json:"email" gorm:"type:varchar(256)"`
	Password string `json:"password" gorm:"type:varchar(500)"`
}

type SignUp struct {
	gorm.Model
	Name     string
	Email    string
	Password string
}
