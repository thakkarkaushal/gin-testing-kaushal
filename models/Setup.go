package models

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
)

// DB Database reference
var DB *gorm.DB

// ConnectDatabase connect to postgres DB
func ConnectDatabase() {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=todo password=todo dbname=todo")
	// defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
	database := db.DB()
	err = database.Ping()
	if err != nil {
		log.Fatal(err)
	}
	db.AutoMigrate(&TodoStruct{}, &User{})
	fmt.Println("Connection was success!")
	DB = db
}
