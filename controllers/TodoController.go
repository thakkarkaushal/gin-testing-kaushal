package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/mountblue/cohort-13-golang/gin-todo-kaushal/models"
	"golang.org/x/crypto/bcrypt"
)

// DisplayTodo godoc
// @Summary Get details of all todo's
// @Description Get details of all todo's present in the database
// @Accept  json
// @Produce  json
// @Success 200 {object} []models.TodoStruct
// @Router /todo [get]
func DisplayTodo(c *gin.Context) {
	var display []models.TodoStruct
	err := models.DB.Table("todo_structs").Select("id,title").Find(&display).Error
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	c.JSON(200, display)
}

// AddTodo godoc
// @Summary Create a new todo
// @Description Create a new todo to the database
// @Accept  json
// @Produce  json
// @Param title body models.TodoStruct true "added todo"
// @Success 200 {object} models.TodoStruct
// @Router /todo [post]
func AddTodo(c *gin.Context) {
	var addStruct models.TodoStruct
	err := c.ShouldBindJSON(&addStruct)
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	err = models.DB.Table("todo_structs").Create(&addStruct).Error
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	c.JSON(200, addStruct)
}

// DeleteTodo godoc
// @Summary Delete a todo
// @Description Delete a todo from the database
// @Accept  json
// @Produce  json
// @Param id path int true "ID"
// @Success 200 {object} models.TodoStruct
// @Router /todo/{id} [delete]
func DeleteTodo(c *gin.Context) {
	var deleteStruct models.TodoStruct
	id := c.Param("id")

	err := models.DB.Table("todo_structs").Where("id=?", id).Delete(&deleteStruct).Error
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	c.JSON(200, gin.H{
		"delete": "Todo item deleted.",
	})
}

// UpdateTodo godoc
// @Summary Update a todo
// @Description Update a todo from the database
// @Accept  json
// @Produce  json
// @Param id path int true "ID"
// @Param title body models.TodoStruct true "Update todo"
// @Success 200 {object} models.TodoStruct
// @Router /todo/{id} [put]
func UpdateTodo(c *gin.Context) {
	var update models.TodoStruct
	id := c.Param("id")
	err := c.ShouldBindJSON(&update)
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	i, err := strconv.ParseUint(id, 10, 64)
	update.ID = uint(i)
	err = models.DB.Save(&update).Error
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	c.JSON(200, update)
}

// Login check user email & password in database
func Login(c *gin.Context) {
	var loginStruct models.User

	type passwordStruct struct {
		Password string
	}
	err := c.ShouldBind(&loginStruct)
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
		return
	}
	var pass passwordStruct
	err = models.DB.Table("users").Where("email = ? ", loginStruct.Email).Find(&pass).Error
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
		return
	}
	fmt.Println(loginStruct)
	output := bcrypt.CompareHashAndPassword([]byte(pass.Password), []byte(loginStruct.Password))
	if output == nil {
		// c.JSON(200, "Login Success")
		c.Redirect(http.StatusTemporaryRedirect, "/home.html")
		return
	} else {
		c.JSON(401, "Cannot login")
		return
	}
}

// SignUp add user email and encrypt password in database
func SignUp(c *gin.Context) {
	var signup models.SignUp
	err := c.ShouldBind(&signup)
	bytes, err := bcrypt.GenerateFromPassword([]byte(signup.Password), 14)
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	signup.Password = string(bytes)
	err = models.DB.Table("users").Save(&signup).Error
	if err != nil {
		c.JSON(401, gin.H{
			"error": err.Error(),
		})
	}
	c.JSON(200, "SignUP Success")
	c.Redirect(http.StatusTemporaryRedirect, "/index.html")
}
