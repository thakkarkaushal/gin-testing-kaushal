// @title TODO API
// @version 1.0
// @description To manage the todo's
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email soberkoder@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:8080
// @BasePath /
// @securityDefinitions.basic BasicAuth

package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	"github.com/gin-contrib/static"
	"gitlab.com/mountblue/cohort-13-golang/gin-todo-kaushal/controllers"
	"gitlab.com/mountblue/cohort-13-golang/gin-todo-kaushal/docs"
	"gitlab.com/mountblue/cohort-13-golang/gin-todo-kaushal/models"
)

func main() {
	server := gin.New()
	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Todo API"
	docs.SwaggerInfo.Description = "Todo list"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	models.ConnectDatabase()
	server.Use(static.Serve("/", static.LocalFile("./templates", true)))
	server.Use(gin.Recovery(), gin.Logger())
	server.GET("/todo", controllers.DisplayTodo)
	server.POST("/todo", controllers.AddTodo)
	server.DELETE("/todo/:id", controllers.DeleteTodo)
	server.PUT("/todo/:id", controllers.UpdateTodo)
	server.POST("/login", controllers.Login)
	server.POST("/signUp", controllers.SignUp)

	server.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	server.Run()
}

func SetupRouter() *gin.Engine{
	server := gin.Default()
	models.ConnectDatabase()
	server.GET("/todo", controllers.DisplayTodo)
	server.POST("/todo", controllers.AddTodo)
	server.DELETE("/todo/:id", controllers.DeleteTodo)
	server.PUT("/todo/:id", controllers.UpdateTodo)
	server.POST("/login", controllers.Login)
	server.POST("/signUp", controllers.SignUp)
	return server
}
