package middleware

import (
	"github.com/gin-gonic/gin"
)

//BasicAuth function provides basic authentication
func BasicAuth() gin.HandlerFunc {
	return gin.BasicAuth(gin.Accounts{
		"admin": "admin",
	})
}
